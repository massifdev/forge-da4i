import dotenv from "dotenv";
dotenv.config();

const clientId = process.env.FORGE_CLIENT_ID || "";
const clientSecret = process.env.FORGE_CLIENT_SECRET || "";
const port = parseInt(process.env.PORT || '3000');
const forgeUrl = process.env.FORGE_URL;

export { clientId, clientSecret, port, forgeUrl }

