// TODO: Incorporate documentation, to make it easier to select engine
interface AppBundle {
    id: string;
    engine: string;
    description: string;
}

export default AppBundle;