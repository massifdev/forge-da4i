import * as express from 'express';
import qs from "qs";
import axios from "axios";
import Auth from './../auth/auth.interface';
import { clientId, clientSecret, forgeUrl } from "./../../config";
import AppBundle from '../appBundles/AppBundle.interface';
import FormData from "form-data";
import fs from 'fs';
import uuid from 'uuid';

export class ForgeService {
        
    static getAccessToken() {
        let auth: Auth = {
            client_id: clientId,
            client_secret: clientSecret,
            grant_type: "client_credentials",
            scope: "code:all data:write data:read bucket:create bucket:delete bucket:read",
        };

        return axios({
            method: 'POST',
            url: `${forgeUrl}/authentication/v1/authenticate`,
            data: qs.stringify(auth)
        });
    }

    static createAppNickName(token: string, nickname: string) {
        return axios({
            method: 'PATCH',
            url: `${forgeUrl}/da/us-east/v3/forgeapps/me`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: {
                nickname: nickname
            }
        });
    }

    static deleteApp(token: string) {
        return axios({
            method: 'DELETE',
            url: `${forgeUrl}/da/us-east/v3/forgeapps/me`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
        });
    }

    static registerAppBundle(token: string, data: AppBundle) {
        return axios({
            method: 'POST',
            url: `${forgeUrl}/da/us-east/v3/appbundles`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: JSON.stringify(data)
        });
    }

    static uploadAppBundle(token: string, filePath: string, formParams: any, uploadUrl: string) {
        const formData = new FormData();
        const formProperties = Object.entries(formParams);
        for (const [key, val] of formProperties) {
            formData.append(key, val);
        }

        formData.append('file', fs.readFileSync(filePath));

        const formHeaders = formData.getHeaders();

        return axios({
            method: 'POST',
            url: uploadUrl,
            headers: {
                'Content-Length': formData.getLengthSync(),
                ...formHeaders
            },
            data: formData
        });
    }

    static aliasAppBundle(token: string, bundleName: string, version: number, alias: string) {

        return axios({
            method: 'POST',
            url: `${forgeUrl}/da/us-east/v3/appbundles/${bundleName}/aliases`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: {
                "version": version,
                "id": alias
            }
        });
    }

    static createActivity(token: string, activityData: any) {
        return axios({
            method: 'POST',
            url: `${forgeUrl}/da/us-east/v3/activities`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: activityData
        });
    }

    static aliasActivity(token: string, id: string, version: number) {
        return axios({
            method: 'POST',
            url: `${forgeUrl}/da/us-east/v3/activities/ChangeParamActivity/aliases`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: {
                "id": id,
                "version": version
            }
        });
    }

    static createBucket(token: string, bucketKey: string) {
        return axios({
            method: 'POST',
            url: `${forgeUrl}/oss/v2/buckets`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: {
                "bucketKey": bucketKey,
                "access": "full",
                "policyKey": "transient"
            }
        });
    }

    static uploadModel(token: string, bucketKey: string, inputFileKey: string, filePath: string) {
        let fileStream = fs.createReadStream(filePath);
        return axios({
            method: 'PUT',
            url: `${forgeUrl}/oss/v2/buckets/${bucketKey}/objects/${inputFileKey}`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": "application/octet-stream"
            },
            data: fileStream
        });
    }

    static getDownloadUrl(token: string, bucketKey: string, inputFileKey: string) {
        return axios({
            method: 'POST',
            url: `${forgeUrl}/oss/v2/buckets/${bucketKey}/objects/${inputFileKey}/signed`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: {}
        });
    }

    static getModelUploadUrl(token: string, bucketKey: string, outputModelFileKey: string) {
        return axios({
            method: 'POST',
            url: `${forgeUrl}/oss/v2/buckets/${bucketKey}/objects/${outputModelFileKey}/signed?access=readwrite`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: {}
        });
    }

    static getImageUploadUrl(token: string, bucketKey: string, outputImageFileKey: string) {
        return axios({
            method: 'POST',
            url: `${forgeUrl}/oss/v2/buckets/${bucketKey}/objects/${outputImageFileKey}/signed?access=readwrite`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: {}
        });
    }

    static createWorkItem(token: string, activityId: string, inputModelUrl: string, inventorParams: { height: string, width: string }, outputModelUrl: string, outputImageUrl: string) {
        let workItemData = {
            activityId: activityId,
            arguments: {
                InventorDoc: {
                    url: inputModelUrl
                },
                InventorParams: {
                    url: `data:application/json,${JSON.stringify({"height": inventorParams.height, "width": inventorParams.width})}`
                },
                OutputIpt: {
                    url: outputModelUrl,
                    verb: "put"
                },
                OutputBmp: {
                    url: outputImageUrl,
                    verb: "put"
                }
            }
        };
        return axios({
            method: 'POST',
            url: `${forgeUrl}/da/us-east/v3/workitems`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },
            data: workItemData
        });
    }

    static getWorkItemStatus(token: string, workItemId: any) {
        return axios({
            method: 'GET',
            url: `${forgeUrl}/da/us-east/v3/workitems/${workItemId}`,
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            }
        });
    }

    static downloadOutputModel(token: string, bucketKey: string, outputModelFileKey: string) {
        return axios({
            method: 'GET',
            url: `${forgeUrl}/oss/v2/buckets/${bucketKey}/objects/${outputModelFileKey}`,
            responseType: "arraybuffer",
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },          
        });
    }

    static downloadOutputImage(token: string, bucketKey: string, outputImageFileKey: string) {
        return axios({
            method: 'GET',
            url: `${forgeUrl}/oss/v2/buckets/${bucketKey}/objects/${outputImageFileKey}`,
            responseType: "arraybuffer",
            headers: {
                authorization: `Bearer ${token}`,
                "Content-Type": 'application/json'
            },          
        });
    }
}