import * as express from "express";
import Controller from "./../controller.interface";
import { ForgeService } from './../services/forge.service';
import { AxiosResponse } from "axios";
import AppBundle from "../appBundles/AppBundle.interface";
import fs from 'fs';
import * as activityJson from './activity.json';
import { v4 } from 'uuid';

class WorkflowController implements Controller {
    path = '/configure';
    public router = express.Router();

    constructor() {
        this.initialiseRoutes();
    }

    public initialiseRoutes() {
        // this.router.post(this.path, ForgeService.getAccessToken);
        this.router.get(`${this.path}/reset`, this.reset);
        this.router.get(`${this.path}/setup`, this.setup);
        this.router.get(this.path, this.runJob);
    }

    setup = async (req: express.Request, res: express.Response) => {

        // Get access token
        const token: string = (await (ForgeService.getAccessToken())).data.access_token;
        console.log(`Token is ${token}`);

        // Create nickname for the app
        let result = await ForgeService.createAppNickName(token, "windowgen");
        console.log("Nickname set to windowgen");

        // Register an app bundle
        let bundleName = "paramChangeBundle"
        let bundleData: AppBundle = {
            id: bundleName,
            engine: "Autodesk.Inventor+24",
            description: "A test app to change params in a model and output a bitmap"
        }

        let bundleRegister = await ForgeService.registerAppBundle(token, bundleData);
        let formData = bundleRegister.data.uploadParameters.formData;
        let uploadUrl = bundleRegister.data.uploadParameters.endpointURL;
        let appBundleVersion = bundleRegister.data.version;

        console.log(`Upload URL: ${uploadUrl}, Form Data: ${JSON.stringify(formData)}`);

        // Upload the app bundle
        let uploadResult = await ForgeService.uploadAppBundle(token, './samplePlugin.bundle.zip', formData, uploadUrl);
        console.log(uploadResult);

        // Create an alias for the app bundle
        let aliasResult = await ForgeService.aliasAppBundle(token, bundleName, appBundleVersion, "current_version");
        console.log('Alias result:', aliasResult);

        // Create an activity
        let activity = await ForgeService.createActivity(token, activityJson);

        // Create an alias for the activity
        let activityResult = await ForgeService.aliasActivity(token, "current", 1);
        console.log(activityResult);

        res.status(200).send("Setup Complete.");

    }

    runJob = async (req: express.Request, res: express.Response) => {
        try {
            // Get access token
            const token: string = (await (ForgeService.getAccessToken())).data.access_token;
            console.log(token);

            // Create a bucket
            let bucketKey = v4();
            try {
                let bucket = await ForgeService.createBucket(token, bucketKey);
                console.log(bucket);
            }
            catch (err) {
                console.log(err)
            }

            // Upload model file to OSS bucket
            let uploadFileKey = v4();
            let modelPath = "./src/workflow/box.ipt";
            let modelUploadResult = await ForgeService.uploadModel(token, bucketKey, uploadFileKey, modelPath);
            console.log(modelUploadResult);

            // Get temp download URL
            let downloadUrl = await ForgeService.getDownloadUrl(token, bucketKey, uploadFileKey);
            let signedUrl = downloadUrl.data.signedUrl;
            console.log(signedUrl);

            // Get temp upload URL for model file
            let outputModelFileKey = v4();
            let tempModelUploadUrl = await ForgeService.getModelUploadUrl(token, bucketKey, outputModelFileKey);
            let signedModelUploadUrl = tempModelUploadUrl.data.signedUrl;
            console.log(signedModelUploadUrl);

            // Get temp upload URL for image
            let outputImageFileKey = v4();
            let tempImageUploadUrl = await ForgeService.getImageUploadUrl(token, bucketKey, outputImageFileKey);
            let signedImageUploadUrl = tempImageUploadUrl.data.signedUrl;
            console.log(signedImageUploadUrl);

            // Create a WorkItem
            let activityId = `windowgen.ChangeParamActivity+current`
            let height: string = `${ req.query.height.toString() } mm`;
            let width: string = `${ req.query.width.toString() } mm`;

            let workItem = await ForgeService.createWorkItem(token, activityId, signedUrl, { height, width }, signedModelUploadUrl, signedImageUploadUrl);
            let workItemId = workItem.data.id;
            console.log(workItem.data);

            // Check status of WorkItem
            let workItemStatus = await this.checkStatus(token, workItemId);
            console.log(workItemStatus);

            // Download reconfigured model file (if required)
            let updatedModel = await ForgeService.downloadOutputModel(token, bucketKey, outputModelFileKey);
            let outputFileName = v4();
            fs.writeFileSync(`${process.cwd()}/public/output/${outputFileName}.ipt`, Buffer.from(updatedModel.data));

            // Download generated image
            let outputImage = await ForgeService.downloadOutputImage(token, bucketKey, outputImageFileKey);
            let outputImageFileName = v4();           
            await fs.writeFileSync(`${process.cwd()}/public/output/${outputImageFileName}.bmp`, Buffer.from(outputImage.data));

            res.status(200).send({"message": "Job Complete", "imagePath": `${outputImageFileName}.bmp`});
        }
        catch (err) {
            console.log(err);
            res.status(500).send(`Ruh roh, big prob!`);
        }

    }

    checkStatus = async (token: string, workItemId: string) => {
        while (true)
        {
            let statusResult = await ForgeService.getWorkItemStatus(token, workItemId);
            let status = statusResult.data.status;
            console.log(statusResult.data.reportUrl);
            if(status != "pending" && status != "inprogress") {
                return status;
            }

            await new Promise((resolve, reject) => setTimeout(resolve, 5000));
        }
    }

    reset = async (req: express.Request, res: express.Response) => {

        // Get access token
        const token: string = (await (ForgeService.getAccessToken())).data.access_token;
        console.log(token);

        // Delete the app
        try {
            let result = await ForgeService.deleteApp(token);
            console.log(result);
        }
        catch (err) {
            console.log(err);
        }

        res.status(200).send("Reset Complete");
    }
}

export default WorkflowController;