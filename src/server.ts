import App from './app';
import * as config from "./../config";
import AuthController from './auth/auth.controller';
import WorkflowController from './workflow/workflow.controller';
 
const app = new App(
  [
    new AuthController(),
    new WorkflowController(),
  ],
  config.port,
);
 
app.listen();