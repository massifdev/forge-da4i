import * as express from "express";
import Controller from "./../controller.interface";
import { ForgeService } from "./../services/forge.service";

class AuthController implements Controller {
    public path: string = '/auth';
    public router = express.Router();

    constructor() {
        this.initialiseRoutes();
    }

    public initialiseRoutes() {
        this.router.post(this.path, ForgeService.getAccessToken);
        this.router.get(this.path, ForgeService.getAccessToken);
        // this.router.get(`${this.path}/refresh`, this.refreshToken); // Use this later for 3-legged auth and refresh
    }

    // refreshToken = (req: express.Request, res: express.Response) => {
    //     let auth: Auth = {
    //         client_id: clientId,
    //         client_secret: clientSecret,
    //         grant_type: "refresh_token",
    //         refresh_token: req.query.refresh_token.toString(),
    //     };

    //     axios({
    //         method: 'post',
    //         url: `${forgeUrl}/authentication/v1/refreshtoken`,
    //         data: qs.stringify(auth)
    //     })
    //         .then((response) => {
    //             res.send(response.data);
    //         })
    //         .catch(err => {
    //             console.log(err);
    //             res.status(500).send(err);
    //         });
    // }
}

export default AuthController;