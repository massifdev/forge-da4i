interface Auth {
    client_id: string;
    client_secret: string;
    grant_type: string;
    scope?: string;
    refresh_token?: string;
}

export default Auth;